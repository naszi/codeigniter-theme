<?php if ( ! defined('THEMEPATH')) exit('No direct script access allowed');

# The default place for javascript in this theme
$config['js_place'] = "footer";

# The default template used in this instance
$config['template'] = 'template_master';

# Bootstrap default stylesheet and javascript included
$config['stylesheets'] = array('bootstrap.css');
$config['javascripts'] = array('jquery_1.11.0.js','bootstrap.min.js');

/* End of file bootstrap.php */
/* Location: /config/theme_settings/bootstrap.php */