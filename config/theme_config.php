<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Theme Base Path
|--------------------------------------------------------------------------
|
| Path to all Theme assets file. Typically this path determines wich version
| currently is used by this library.
|
| Note: This is important, de dault configuration of the instance depends of
| the THEMEPATH.
|
*/

define('THEMEPATH', 'theme');

/*
|--------------------------------------------------------------------------
| Custom Instance Paths for asset_helper.php 
|--------------------------------------------------------------------------
|
| Path to all Theme assets file used INSIDE this library. Basically you don't
| need to configure this settings unless you are placing the assets in another
| folder outisde this library.
|
| Important: DO NOT replace the {instance} variable, it is used inside the Theme 
| Library for identify the actual instance and get the instance assets paths.
|
*/

$config['instance_paths']['asset_path'] 	= THEMEPATH.'/{instance}';
$config['instance_paths']['css_path'] 		= THEMEPATH.'/{instance}/css';
$config['instance_paths']['less_path'] 		= THEMEPATH.'/{instance}/less';
$config['instance_paths']['js_path'] 		= THEMEPATH.'/{instance}/js';
$config['instance_paths']['img_path'] 		= THEMEPATH.'/{instance}/img';
$config['instance_paths']['swf_path'] 		= THEMEPATH.'/{instance}/swf';
$config['instance_paths']['xml_path'] 		= THEMEPATH.'/{instance}/xml';
$config['instance_paths']['template_path']  = '{instance}/templates';
$config['instance_paths']['layout_path']	= '{instance}/layouts';

/*
|--------------------------------------------------------------------------
| Custom Asset Paths for asset_helper.php
|--------------------------------------------------------------------------
|
| URL Paths to static assets library. You can append assets outisde this 
| library, in here you can configure the paths. The default path is assets/
| outside the application directory.
|
*/

$config['asset_path'] = 'assets';
$config['css_path']   = 'assets/css';
$config['less_path']  = 'assets/less';
$config['js_path'] 	  = 'assets/js';
$config['img_path']   = 'assets/img';
$config['swf_path']   = 'assets/swf';
$config['xml_path']   = 'assets/xml';

/*
|--------------------------------------------------------------------------
| Theme Instance Settings Path
|--------------------------------------------------------------------------
|
| Default theme settings path, in this dir you can create as many theme 
| settings as you want.
|
*/

$config['instance_settings_path'] = 'instance_settings/';

/*
|--------------------------------------------------------------------------
| Theme Default Instance 
|--------------------------------------------------------------------------
|
| The default instance used.
|
*/

$config['default_instance'] = 'bootstrap';

/* End of file theme_config.php */
/* Location: /config/theme_config.php */