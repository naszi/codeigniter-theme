Codeigniter Theme
---
This is a Codeigniter Library for render a standard HTML layout using any framework as a base such as 'Twitter Bootstrap','Foundation', etc.

Summary
---
This is the traditional way to create a template in *CodeIgniter*:

	<?php 
	 $data['css'] = array('bootstrap.css','custom.css');
	 $this->load->view('template/header',$data);
	 $this->load->view('template/menu');
	 $this->load->view('template/sidebar');
	 $this->load->view('dashboard');
	 $data['javascript'] = array('jquery.js','bootstrap.js','dashboard.js');
	 $this->load->view('template/footer',$data);

As you see, there is integrated the graphical user interface logic in each view individually.

**Disadvantages of the traditional way:** 

* It is quite difficult to create a standard when you have a very robust site with many views per call. 
* Much time is lost when you have to implement the same logic in each function. 
* It is almost impossible to get away a bit of the standard created because slight changes would affect all the different views. 
* It is very difficult to detect errors in many files.

**Using Codeigniter Theme:** 
	
	<?php 
	$this->theme->add('view','dashboard')->render();

All that the theme needs, such as:

* css
* js
* titles

It will be configured depending on the preloaded standard.

Features 
---

* Not rewrite any library used by CodeIgniter, eg: change the way you use **$this-> load->view()** by **$this->load->view_theme()** or something like that. 
* All topics and their assets in one place, separate from the library directory, this includes: view, css, js, etc. 
* You can call assets from elsewhere outside the library directory. 
* No need to declare a path (url) each time you load a file css, js, img, etc.
* You can install any type of Framework, eg Twitter Bootstrap, Foundation, Skeleton, etc. 
* You can include css and js dynamically in a controller function without affecting the others. 
* You can change the instance (framework) of any controller without affecting the others. 
* You can create multiple instances.

Codeigniter Theme comes preloaded with the assets paths that will be used by default inside and outside the library, the assets do not necessarily have to be inside the library directory, only the default assets of the loaded instance should be placed within the library directory.

Installation (4 steps)
---

1.- Install with composer

	"require": {
		"dsv/codeigniter-theme": "dev-master"
	}
2.- Unfortunately there is no direct installer for "assets" used in codeigniter and this library has the "assets" directory in a place not indicated, we need to create a symbolic access outside the application directory for this to work, in your codeigniter project execute:

	ln -s application/third_party/codeigniter-theme/assets/ theme
    
3.- Adding a package path in your controller:

	<?php 
	$this->load->add_package_path(APPPATH.'third_party/codeigniter-theme');
    
4.- Load the library: 

	<?php 
	$this->load->library('theme');
	
Optional: you can create a core controller (usually called MY_Controller) and call the Codeigniter Theme functions then extend in your controller as follows:

	class MY_Controller extends CI_Controller {
		
		function __construct()
		{
			parent::__construct();
			$this->load->add_package_path(APPPATH.'third_party/codeigniter-theme');
			$this->load->library('theme');
		}
	}

For more information please check: http://ellislab.com/codeigniter/user-guide/general/core_classes.html

## And we are good to go

An object to be used within the controller is created:

	<?php
	$this->theme

Examples
---
Adding 1 view:

    <?php
    $this->theme->add('view','dashboard')->render();

Adding 1 view with parameters:
	
	<?php
	$data['params'] = $this->random_model->get_all();
	$this->theme->add('view','dashboard',$data)->render();

Adding 2 views as array:

    <?php 
    $this->theme->add('view',array('dashboard','table_view'))
                ->render();

Adding css y javascript:
	<?php
	$this->theme->add('css','custom.css')
				->add('js','custom.js')
				->add('view','dashboard')
				->render();
Set title:

	<?php
	$this->theme->set('title','Dashboard | Codeigniter')
				->add('view','dashboard')
				->render();

Change the instance inside a controllers with different functions:

	class Test extends CI_Controller { 
		public function index(){
			$this->theme->set('instance','foundation')
						->add('view','dashboard')
						->render();
		}
		public function test(){
			$this->theme->set('instance','bootstrap')
						->add('view','dashboard')
						->render();
		}
	}

How to install a new theme (instance)
---

In codeigniter-theme third-party library you need to upload the elements in the order as follows:

	assets/
		* {instance_name}/
			** css
			** js
	config/
		* instance_settings/
			** {instance_name}		
	views/
		* {instance_name}
			** templates/
			** layouts/

The system comes preloaded with some of the most used by the community Frameworks (eventually will add more frameworks): 

* Twitter Bootstrap 

Check configuration files and directories assets and views to a practical example of installation.

Change Log
---
###Version 0.0.4 

* New template system, columns and directives. 
* Moved some configuration variables, making it easier to create new threads. 
* Changing javascripts dynamically place (header / footer). 
* New method to parse the data from the render function. 

###Version 0.0.3 

* New method to change the css media files 
* Fix bugs (add function parameters) 

###Version 0.0.2 

* Add multiple css, js and views as array from a single call.