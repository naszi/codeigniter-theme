<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{theme_title}</title>
      {theme_css}
        {stylesheet}
      {/theme_css}
      {theme_js_header}
        {javascript}
      {/theme_js_header}
  </head>
  <body>
      {theme_body}
        {content}
      {/theme_body}
      {theme_js_footer}
        {javascript}
      {/theme_js_footer}
  </body>
</html>