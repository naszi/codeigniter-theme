<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package CodeIgniter
 * @author  ExpressionEngine Dev Team
 * @copyright  Copyright (c) 2006, EllisLab, Inc.
 * @license http://codeigniter.com/user_guide/license.html
 * @link http://codeigniter.com
 * @since   Version 2.1.4
 * @filesource
 */

// --------------------------------------------------------------------

/**
 * CodeIgniter Theme Class
 *
 * Templating is done by layering the standard CI view system and extending 
 * it to asset management. The basic idea is that for every single CI view 
 * there are individual CSS and Javascript files that correlate to it.
 *
 * @package		CodeIgniter
 * @author		David Sosa Valdes
 * @subpackage	Libraries / Sparks
 * @category	Libraries / Sparks
 * @link		
 * @copyright   Copyright (c) 2014, David Sosa Valdes.
 * @version 	0.0.5
 * 
 */

class Theme 
{
	public $title;
	private $stylesheets;
	private $javascripts;
	private $instance;
	private $settings;
	private $paths;
	private $views;
	private $layout;
	private $template;
	private $settings_path;
	private $directives;
	protected $CI;

	/**
	* Theme::__construct()
	* ------------------------------------------------------------------
	* Loads instance settings, template, layouts, insert js and css 
	* and validates existence of default instance.
	* 
	* @access public
	* @return void
	*/

	function __construct($params = array())
	{
		# Set the Codeigniter instance and load some helpers/libraries
		$this->CI =& get_instance();
		$this->CI->config->load('theme_config');
		$this->CI->load->library('parser');
		$this->CI->load->helper(array('asset_helper','file','url'));
		# Initialize the default template asset paths
		$this->settings_path = $this->CI->config->item('instance_settings_path');
		$default_instance = $this->CI->config->item('default_instance');
		$this->instance = (isset($params['instance']))? $params['instance']: $default_instance;
		$this->settings_path .= $this->instance;
		# Set the settings/assets path of theme instance selected
		$this->CI->config->load($this->settings_path,TRUE);
		$this->settings = $this->CI->config->item($this->settings_path);
		if(isset($params["instance_paths"])){
			$instance_paths = $params["instance_paths"];
		} else {
			$instance_paths = $this->CI->config->item('instance_paths');	
		}
		$this->paths = $this->_set_paths($instance_paths);
		# Initialize the css and js arrays
		$this->stylesheets = array();
		$this->javascripts = array();
		$this->javascripts['header'] = $this->javascripts['footer'] = array();
		# Initialize the views
		$this->views = array();
		# Set the default title to the header template
		$this->set('title');
		# Add the default css
		$this->add('css',$this->settings['stylesheets'], $this->paths["css_path"]);
		# Add the default js 
		$this->add('js', $this->settings['javascripts'], $this->paths["js_path"]);
		# Set the default template/layout logic
		$this->template = (isset($params["template"]))? $params['template'] : $this->settings['template'];
		$this->layout = (isset($params["layout"]))? $params['layout'] : NULL;
		# Set the default directives
		$this->directives = array(
			"ALL" => array($this,'_all_directive_parser')
		);
	}

	/**
	* Theme::set()
	* ------------------------------------------------------------------
	* 
	* Set some properties used in the template:
	* 
	* 	- title 	: Set the title inside the headers of the template.
	*	- instance 	: change the instance used by default, you can pass an array
	*			  	  with properties used by this new instance.
	*	- js 		: Set js place used by default (header/footer).
	*	- layout 	: Set the default layout used in the current controller, need
	*				  the actual layout view declared inside the layout directory
	*				  inside theme/views/{instance}/layouts/{layout_view} library
	*				  and the tags with the views used for this layout.
	*
	* @access public
	* @param  string $type
	* @param  string $param 
	* @param  bool|array $optional  
	* @return $this - returns the current object (fluent interface)
	*/

	public function set($type = '', $param = '', $optional = NULL)
	{
		switch ($type) {
			case 'title':
				$title = (!is_null($param))? $param : $this->CI->router->fetch_class();
				# We need to give it some style
				$this->title = ucwords($title);
				break;
			case 'instance':
				$instance = (!is_null($param))? $param : $this->CI->config->item('default_instance');
				# This is the way that we need to call the instance and get the properties
				$instance_call = array('instance' => $instance);
				# Is there some extra params declared, like  ?
				$extra_params = (is_array($optional))? $optional : array();
				$this->__construct(array_merge($instance_call,$extra_params));
				break;
			case 'js':
				$place = (!is_null($param))? $param : $this->settings['js_place'];
				$this->settings['js_place'] = $place;
				# All javascripts forced to move?
				if ($optional === TRUE) {
					$this->_change_js_place();	
				}
				break;
			case 'layout':
				# We need the parameters
				if (!is_null($param) && is_array($optional)) {
					list($name,$content) = array($param,$optional);
					$this->layout = array($name => $content);
				}
				break;
		}
		return $this;
	}

	/**
	* Theme::add()
	* ------------------------------------------------------------------------
	* Add some files used in the template:
	*
	*	- css  : add many css files as you want to the header place inside the template. 
	*			 You can set the css path if you need and pass as the $params parameter. 
	*	- js   : add js files to the header/footer set by the default place inside 
	*			 the template you can set the css path if you need and pass as the 
	*			 $params parameter. 
	*	- view : add many views as you want, you can pass data like the load->view
	*			 method in Codeigniter with the $params parameter.
	*
	* @access public
	* @param string $type
	* @param string|array $files - you can declare one single file as strong or many as array.
	* @param string|bool|array $params 
	* @return $this - returns the current object (fluent interface)
	*/

	public function add($type = '', $files = '', $params = FALSE)
	{
		foreach ((array) $files as $key => $file) {
			switch ($type) {
				case 'css':
					$css_path = $params;
					$this->stylesheets[] = css($file,$css_path);
					break;
				case 'js':
					$place = $this->settings['js_place'];
					$js_path = $params;
					$this->javascripts[$place][] = js($file,$js_path);				
					break;
				case 'view':
					$this->views[$file] = $params; 	
					break;
			}
		}
		return $this;
	}

	/**
	* Theme::render()
	* ------------------------------------------------------------------------
	* Call and join all the data needed for render the template.
	*
	* Check if the template exist inside the template folder inside the instance.
	* If not set the template master.
	*
	* @access public
	* @return void	
	*/

	public function render()
	{
		$data = array(
			"theme_title" 		=> $this->title,
			"theme_css" 		=> $this->_create_parsedata('css'),
			"theme_js_header" 	=> $this->_create_parsedata('js_header'),
			"theme_js_footer" 	=> $this->_create_parsedata('js_footer'),
			"theme_body" 		=> $this->_create_parsedata('body')
		);
		$pos_view = $this->paths['template_path'].'/'.$this->template;
		$template_view = (get_file_info($pos_view) !== FALSE)? $pos_view : 'template_master';
		$this->CI->parser->parse($template_view, $data);
	}

	/**
	* Theme::_create_parsedata()
	* ------------------------------------------------------------------------
	* Call and join all the data needed for render the template.
	*
	* @access private
	* @param string $type
	* @return void	
	*/

	private function _create_parsedata($type = '',$tag = NULL)
	{
		$parsedata = array();
		switch ($type) {
			case 'css':
				$tag = (!is_null($tag))? $tag : 'stylesheet';
				foreach ($this->stylesheets as $key => $value) {
					$parsedata[] = array($tag => $value);
				}
				$number_values = count($this->stylesheets);
				break;
			case 'js_header':
				$tag = (!is_null($tag))? $tag : 'javascript';
				foreach ($this->javascripts['header'] as $key => $value) {
					$parsedata[] = array($tag => $value);
				}
				$number_values = count($this->javascripts['header']);
				break;
			case 'js_footer':
				$tag = (!is_null($tag))? $tag : 'javascript';
				foreach ($this->javascripts['footer'] as $key => $value) {
					$parsedata[] = array($tag => $value);
				}
				$number_values = count($this->javascripts['footer']);
				break;
			case 'body':
				$tag = (!is_null($tag))? $tag : 'content';
				# Is there a layout declared?
				if (!is_null($this->layout)) {
					$parsedata[] = $this->_construct_layout($tag);
				}
				# We need to set all views together inside the body content tag
				else {
					foreach ($this->views as $view => $params) {
						$parsedata[] = array($tag => $this->CI->load->view($view, $params, TRUE));
					}
				}
				$number_values = count($this->views);
				break;
		}
		# We need to declared empty when there is no data because the CI parse library
		# needs some data to parse, else print the parser tag inside the view.
		if ($number_values <= 0) {
			$parsedata[] = array($tag => '');
		}
		return $parsedata;
	}

	/**
	* Theme::_construct_layout()
	* ------------------------------------------------------------------------
	* Call and join all the data needed inside the views for render the layout.
	*
	* @access private
	* @param string $tag - the tag declared as the body content tag.
	* @return array
	*/

	private function _construct_layout($tag = '')
	{
		$name = key($this->layout);
		$layout = $this->layout[$name];
		$sub_parsedata = array();
		foreach ($layout as $layout_tag => $layout_view) {
			if (array_key_exists($layout_view, $this->views)) {
				$params = $this->views[$layout_view];
				$sub_parsedata[$layout_tag] = $this->CI->load->view($layout_view,$params,TRUE);
				unset($this->views[$layout_view]);
			} 
			elseif (array_key_exists($layout_view, $this->directives)) {
				$directive = $this->directives[$layout_view];
				call_user_func_array($directive, array(&$sub_parsedata,$layout_tag,$layout_view));
			}
		}
		$layout_view = $this->paths['layout_path']."/".$name;
		return array($tag => $this->CI->parser->parse($layout_view,$sub_parsedata,TRUE));		
	}

	/**
	* Theme::_all_directive_parser()
	* ------------------------------------------------------------------------
	* This directive is used for append all the parse data of all views declared.
	* It is called by the tag ALL declared in the layout.
	*
	* Important: this function is only used when we need to construct a layout.
	*
	* @access private
	* @param reference|array $sub_parsedata - the array used for the parse data.
	* @return array
	*/

	private function _all_directive_parser(&$sub_parsedata, $layout_tag, $layout_view)
	{
		foreach ($this->views as $view => $params) {
			$content = $this->CI->load->view($view,$params,TRUE);
			$sub_parsedata[$layout_tag][] = array($layout_view => $content);
		}
		return $sub_parsedata;		
	}

	/**
	* Theme::_change_js_place()
	* ------------------------------------------------------------------------
	* Change all the javascripts from header to footer and viceversa.
	* 
	* @access private
	* @return void
	*/

	private function _change_js_place()
	{
		switch ($this->settings['js_place']) {
			case 'header':
				$this->javascripts['header'] = $this->javascripts['footer'];
				$this->javascripts['footer'] = array();
				break;
			case 'footer':
				$this->javascripts['footer'] = $this->javascripts['header'];
				$this->javascripts['header'] = array();
				break;
		}
	}

	/**
	* Theme::_set_paths()
	* ------------------------------------------------------------------------
	* Replace the {instance} constant with the instance used.
	* 
	* @access private
	* @param array $instance_paths - all the paths declared in theme_config or in
	*								 the construct.
	* @return array
	*/

	private function _set_paths($instance_paths)
	{
		$paths = array();
		foreach ($instance_paths as $key => $path) {
			$paths[$key] = str_replace("{instance}", $this->instance, $path);
		}
		return $paths;
	}
}

/* End of file Theme.php */
/* Location: /libraries/Theme.php */