<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Get asset URL
 *
 * @access  public
 * @return  string
 */

if ( ! function_exists('asset_url'))
{
    function asset_url($path = FALSE)
    {
        //get an instance of CI so we can access our configuration
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }
        //return the full asset path
        return base_url($CI->config->item('asset_path'));
    }
}

/**
 * Get css URL
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('css_url'))
{
    function css_url($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }        
        return base_url($CI->config->item('css_path'));
    }
}

/**
 * Get less URL
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('less_url'))
{
    function less_url($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }        
        return base_url($CI->config->item('less_path'));
    }
}

/**
 * Get js URL
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('js_url'))
{
    function js_url($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }        
        return base_url($CI->config->item('js_path'));
    }
}

/**
 * Get image URL
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('img_url'))
{
    function img_url($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }        
        return base_url($CI->config->item('img_path'));
    }
}

/**
 * Get SWF URL
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('swf_url'))
{
    function swf_url($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }        
        return base_url($CI->config->item('swf_path'));
    }
}

/**
 * Get Upload URL
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('upload_url'))
{
    function upload_url($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }        
        return base_url($CI->config->item('upload_path'));
    }
}

/**
 * Get Download URL
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('download_url'))
{
    function download_url($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }        
        return base_url($CI->config->item('download_path'));
    }
}

/**
 * Get XML URL
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('xml_url'))
{
    function xml_url($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return base_url($path);
        }        
        return base_url($CI->config->item('xml_path'));
    }
}


// ------------------------------------------------------------------------
// PATH HELPERS

/**
 * Get asset Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('asset_path'))
{
    function asset_path($path = FALSE)
    {
        //get an instance of CI so we can access our configuration
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('asset_path');
    }
}

/**
 * Get CSS Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('css_path'))
{
    function css_path($path = FALSE)
    {
        //get an instance of CI so we can access our configuration
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('css_path');
    }
}

/**
 * Get LESS Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('less_path'))
{
    function less_path($path = FALSE)
    {
        //get an instance of CI so we can access our configuration
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('less_path');
    }
}

/**
 * Get JS Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('js_path'))
{
    function js_path($path = FALSE)
    {
        //get an instance of CI so we can access our configuration
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('js_path');
    }
}

/**
 * Get image Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('img_path'))
{
    function img_path($path = FALSE)
    {
        //get an instance of CI so we can access our configuration
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('img_path');
    }
}

/**
 * Get SWF Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('swf_path'))
{
    function swf_path($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('swf_path');
    }
}

/**
 * Get XML Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('xml_path'))
{
    function xml_path($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('xml_path');
    }
}

/**
 * Get the Absolute Upload Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('upload_path'))
{
    function upload_path($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('upload_path');
    }
}

/**
 * Get the Relative (to app root) Upload Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('upload_path_relative'))
{
    function upload_path_relative($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return './'.$path;
        }        
        return './' . $CI->config->item('upload_path');
    }
}

/**
 * Get the Absolute Download Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('download_path'))
{
    function download_path($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return FCPATH . $path;
        }        
        return FCPATH . $CI->config->item('download_path');
    }
}

/**
 * Get the Relative (to app root) Download Path
 *
 * @access  public
 * @return  string
 */
if ( ! function_exists('download_path_relative'))
{
    function download_path_relative($path = FALSE)
    {
        $CI =& get_instance();
        if ($path !== FALSE) {
            return './' . $path;
        }        
        return './' . $CI->config->item('download_path');
    }
}


// ------------------------------------------------------------------------
// EMBED HELPERS

/**
 * Load CSS
 * Creates the <link> tag that links all requested css file
 * @access  public
 * @param   string
 * @return  string
 */
if ( ! function_exists('css'))
{
    function css($file, $path = FALSE, $media='all')
    {
        return '<link rel="stylesheet" type="text/css" href="' . css_url($path) ."/". $file . '" media="' . $media . '">'."\n";
    }
}

/**
 * Load LESS
 * Creates the <link> tag that links all requested LESS file
 * @access  public
 * @param   string
 * @return  string
 */
if ( ! function_exists('less'))
{
    function less($file, $path = FALSE)
    {
        return '<link rel="stylesheet/less" type="text/css" href="' . less_url($path) ."/". $file . '">'."\n";
    }
}

/**
 * Load JS
 * Creates the <script> tag that links all requested js file
 * @access  public
 * @param   string
 * @param 	array 	$atts Optional, additional key/value attributes to include in the SCRIPT tag
 * @return  string
 */
if ( ! function_exists('js'))
{
    function js($file, $path = FALSE, $atts = array())
    {
        $element = '<script type="text/javascript" src="' . js_url($path) ."/". $file . '"';

		foreach ( $atts as $key => $val )
			$element .= ' ' . $key . '="' . $val . '"';
		$element .= '></script>'."\n";

		return $element;
    }
}
/* End of file asset_helper.php */
/* Location: /helpers/asset_helper.php */